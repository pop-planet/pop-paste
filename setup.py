#!/usr/bin/python3

"""
 Copyright 2018 Ian Santopietro <isantop@gmail.com>
 Copyright 2018 Pop!_Planet Team <team@pop-planet.info>

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
"""


from distutils.core import setup

setup(
    name="pop-paste",
    version="1.0.0",
    description="Paste to Pop!_Planet's Pastebin from the command line!",
    url="https://gitlab.com/pop-planet/pop-paste",
    author="Ian Santopietro",
    author_email="isantop@gmail.com",
    maintainer="Pop!_Planet Team",
    maintainer_email="support@pop-planet.info",
    license="ISC",
    packages=["pop_paste"],
    scripts=["bin/pop-paste"],
    data_files=[
        ("share/doc/pop-paste", ["debian/copyright"]),
        ("share/man/man1", ["docs/pop-paste.1"]),
    ],
)
