# Changelog

## 1.0.1 (2020-02-29)

* Fixed: Undeclared variable that broke --help

## 1.0.0 (2020-02-22)

* Added: Code quality checks
* Fixed: Bug in writing paste to file
* Improved: Code quality

## 0.0.3 (2019-01-01)

* Added: Config file to allow overriding default settings
* Added: Direct file input
* Added: Get (-g) option with optional raw download and direct file output
* Fixed: Make the -E option actually work
* Improved: Better handling for outputting URLs for pastes with '-e burn'

## 0.0.2 (2018-12-20)

* Fixed: Make the application work functionally

## 0.0.1 (2018-12-20)

* First official release!
