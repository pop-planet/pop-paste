#!/usr/bin/python3

"""
pop-paste - A simple CLI Utility to paste text to the Pop!_Planet pastebin.

Copyright 2018 Ian Santopietro <isantop@gmail.com>
Copyright 2018 Pop!_Planet Team <team@pop-planet.info>
"""

import json
from urllib import request as req
from urllib import parse
from urllib import error


class Paste:
    """
    The Paste class for pop-paste.
    """

    pastebin = "https://paste.pop-planet.info"
    api_url = "{}/api".format(pastebin)
    create_url = "{}/create".format(api_url)
    get_url = "{}/paste".format(api_url)
    raw_url = "{}/view/raw".format(pastebin)
    paste_url = ""

    @classmethod
    def get_encoded_paste(self, data):
        """
        Returns a urlencoded paste from the dictionary "data"
        """
        return parse.urlencode(data).encode()

    def send_paste(self, data):
        """
        Sends out a paste to the correct pastebin
        """
        request = req.Request(self.create_url, data=data)
        response = req.urlopen(request)
        return response.read().decode("utf-8")

    def get_paste(self, paste_id):
        """
        Gets an existing paste based on a specified ID
        """
        request = req.Request("{}/{}".format(self.get_url, paste_id))
        response = req.urlopen(request)
        response = json.loads(response.read().decode("utf-8"))

        if "paste" in response:
            del response["paste"]

        return response

    def get_raw(self, paste_id):
        """
        Gets an existing paste based on a specified ID (raw)
        """
        request = req.Request("{}/{}".format(self.raw_url, paste_id))

        try:
            response = req.urlopen(request)
        except error.HTTPError as e:
            return "{}: {}".format(e.code, e.reason)

        return response.read().decode("utf-8-sig")

    def get_langs(self):
        """
        Gets a dict of supported languages from the pastebin
        """
        request = req.Request("{}/langs".format(self.api_url))
        response = req.urlopen(request)
        return json.loads(response.read().decode("utf-8"))
